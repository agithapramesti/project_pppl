package com.teknikinformatika.tugaspppl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TugasppplApplication {

	public static void main(String[] args) {
		SpringApplication.run(TugasppplApplication.class, args);
	}
}
